# SEPA_FPGA_GIERM

Entrega de las prácticas de la asignatura SEPA, 4º curso de GIERM.


## Visuals
Dejamos un vídeo demostración en el siguiente enlace:
	

## File location
	**/binaries**
	Tan solo entregamos un archivo **.bit** para la parte **HW** ya que incluye todos los utilizados para las prácticas.
	Se adjuntan los archivos **.bin** en una correspondiente a cada práctica y de la que se encuentran los main en las 
	carpetas de ruta "/neorv32/sw/example" con el mismo nombre de carpeta.
	
	**/neorv32**
	Se adjunta la carpeta con todos los archivos actualizados. Los archivos modificados son el Top Level: 
	"/neorv32/setups/osflow/board_tops" de nombre neorv32_iCEBreaker_BoardTop_MinimalBoot.vhd
	Los archivos de la carpeta "/neorv32/rtl/periph" que son todos nuestros periféricos.
	Por último, el archivo filesets.mk de la carpeta "/home/salas/entrega_FPGA/neorv32/setups/osflow"
	
## Authors and acknowledgment
 Miguel Gil Castilla 
 Dame Seck Diop

### :copyright: Legal

* [Overview](https://stnolting.github.io/neorv32/#_legal) - license, disclaimer, proprietary notice, ...
* [Citing](https://stnolting.github.io/neorv32/#_citing) - citing information (DOI)
* [Impressum](https://github.com/stnolting/neorv32/blob/master/docs/impressum.md) - imprint (:de:)

[[back to top](#The-NEORV32-RISC-V-Processor)]


## Project status
Las prácticas se encuentran listas para su corrección.

